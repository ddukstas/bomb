import { IExplosiveConfig } from "./prepare";
import { IShatterConfig } from "./createShatter";

export type TCoord = {x: number, y: number};

export const purge = <T extends Object>(o: T) => {
    for (let key in o) {
        if (o.hasOwnProperty(key)) {
            switch (typeof o[key]) {
                case 'undefined':
                    delete o[key];
                    break;
                case 'object':
                    o[key] = purge(o[key]);
                    break;
            }
        }
    }
    return o;
}

export const inferShatterConfig = (config: IExplosiveConfig = {}) => 
    purge({
        duration: config.duration,
        distance: config.distance,
        center: config.center,
        color: config.color,
        class: config.shatterClass
    });

export const getShatterColor = (el: Element) => 
    window.getComputedStyle(el).getPropertyValue('background-color') ||
    '#CCCCCC';

export const getShatterSize = (
    rect: ClientRect, 
    sliceCount = 10, 
    maxSliceSize = 15
) => ({
    x: Math.min(rect.width  / sliceCount, maxSliceSize), 
    y: Math.min(rect.height / sliceCount, maxSliceSize)
});

export const getElementCenter = (rect: ClientRect) => ({
    x: (rect.left + rect.right) / 2, 
    y: (rect.top + rect.bottom) / 2
});
