import { IExplosiveConfig, prepare } from './prepare';

export {prepare} from './prepare';

export const explode = (
    el: Element, 
    config?: IExplosiveConfig
) => {
    const detonate = prepare(el, config);
    requestAnimationFrame(() => detonate());
};
