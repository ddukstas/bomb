import { TCoord } from "./utils";

export interface IShatterConfig {
    pos: TCoord;
    size: TCoord;
    center: TCoord;
    duration: number;
    color?: string;
    distance?: number;
    class?: string;
}

const getDirection = (pos: TCoord, center: TCoord, distance: number) => ({
    x: (pos.x - center.x) * Math.random() * distance, 
    y: (pos.y - center.y) * Math.random() * distance
});

const setShatterStyle = (
    el: Element, 
    config: IShatterConfig, 
    progress: number
) => {
    const s = 1 + 2 * progress;
    const r = Math.round(progress * 180 * Math.random());
    const o = 1 - progress;
    const direction = getDirection(config.pos, config.center, config.distance);
    const t = {
        x: direction.x * progress, 
        y: direction.y * progress
    };
    const d = config.duration;
    
    if (config.class) {
        el.setAttribute('class', config.class);
    }

    el.setAttribute('style', `
        left: ${config.pos.x}px;
        top:  ${config.pos.y}px;
        height: ${config.size.x}px;
        width:  ${config.size.y}px;
        transform: translate(${t.x}px, ${t.y}px) scale(${s}) rotate(${r}deg);
        opacity: ${o};
        position: absolute;
        background: ${config.color || '#6f5d4c'};
        pointer-events: none;
        transition: all linear ${d}ms, opacity ${d}ms;
        visibility: ${progress ? 'visible' : 'hidden'}
    `);
};

export const createShatter = (config: IShatterConfig) => {
    const el = document.createElement('div');
    setShatterStyle(el, config, 0);
    document.body.appendChild(el);

    const detonate = (configChanges?: IShatterConfig) => {
        Object.assign(config, configChanges);
        setShatterStyle(el, config, 1);
        setTimeout(() => document.body.removeChild(el), config.duration);   
    };
    
    return detonate;
}
