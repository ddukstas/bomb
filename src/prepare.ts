import { createShatter } from "./createShatter";
import { inferShatterConfig, getShatterSize, getElementCenter, getShatterColor, TCoord } from "./utils";

export interface IExplosiveConfig {
    center?: TCoord;
    shouldRemoveEl?: boolean;
    color?: string;
    duration?: number;
    distance?: number;
    sliceCount?: number;
    maxSliceSize?: number;
    shatterClass?: string;
}

export const prepare = (
    el: Element, 
    config: IExplosiveConfig = {}
) => {
    const rect = el.getBoundingClientRect();

    // set fallback config values
    Object.assign(config, {
        center: config.center || getElementCenter(rect),
        color: config.color || getShatterColor(el),
        duration: config.duration || 500,
        distance: config.distance || 2,
    });

    const size = getShatterSize(rect, config.sliceCount, config.maxSliceSize);
    const commonConfig = Object.assign(inferShatterConfig(config), {size});
    const partDetonators = [];
    for (let x = rect.left; x < rect.right; x += size.x) {
        for (let y = rect.top; y < rect.bottom; y += size.y) {
            const pos = {x, y};
            const detonate = createShatter({...commonConfig, pos});
            partDetonators.push(detonate);
        }
    }
    
    const detonate = (configChanges?: IExplosiveConfig) => {
        const partConfigChanges = inferShatterConfig(configChanges);
        partDetonators.forEach(d => d(partConfigChanges));

        const newConfig = Object.assign({}, config, configChanges);
        if (newConfig.shouldRemoveEl) {
            el.parentElement.removeChild(el);
        } else {
            el.setAttribute('style', 'visibility: hidden');
        }
    };

    return detonate;
}
