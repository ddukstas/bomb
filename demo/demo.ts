import {prepare} from '../src/index';

const EMOJIS = [':)', ':o', '>:|', ':<', ':>'];
const createBox = () => {
    const l = 10 + Math.round(Math.random() * 20);
    const x = Math.round(Math.random() * window.innerWidth  - l/2);
    const y = Math.round(Math.random() * window.innerHeight - l/2);
    const isRound = Math.random() > .5;
    const rotation = Math.round(Math.random() * 360);
    const hue = Math.round(Math.random() * 360);
    const color = `hsl(${hue}, 100%, 30%)`;

    const el = document.createElement('div');
    el.setAttribute('class', 'box');
    el.setAttribute(
        'style', 
        `background: hsl(${hue}, 60%, 50%); ` + 
        `border-color: ${color}; ` + 
        `color: ${color}; ` + 
        `height: ${l}vw; width: ${l}vw; ` + 
        `left: ${x}px; top: ${y}px; position: absolute;` + 
        `transform: rotate(${rotation}deg); ` +
        (isRound ? 'border-radius: 50%' : '')
    );
    el.innerHTML = EMOJIS[Math.round(Math.random() * (EMOJIS.length - 1))];
    document.body.appendChild(el);

    // place explosives onto the element
    const detonate = prepare(el, {
        shouldRemoveEl: true,
        distance: 2,
        sliceCount: 10,
        maxSliceSize: 15,
        shatterClass: 'asdf'
    });

    // detonate on click!
    el.addEventListener('click', e => detonate({
        center: {x: e.clientX, y: e.clientY}
    }));
}

setInterval(() => {
    let boxCount = document.querySelectorAll('.box').length;
    while(boxCount < 5) {
        createBox();
        boxCount++;
    }
}, 500);
