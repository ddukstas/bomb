# Bomb 💣

`npm i @ddlab/bomb`

A tiny function which creates a visual explosion effect of DOM elements.

![💣](demo.gif)

### Simple example
```typescript
import {explode} from '@ddlab/bomb';
const config = ... // for config options look at example below

const el = document.querySelector('.box');
el.addEventListener('click', e => explode(el, config));
```

### Performant example
Use *prepare* -> *detonate* sequence to save cpu cycles during animation. 
```typescript
import {prepare} from '@ddlab/bomb';
const config = ... // for config options look at example below

const el = document.querySelector('.box');

// place explosives onto the element
const detonate = prepare(el, config);

// detonate on click!
el.addEventListener('click', e => {
    const configChanges = {
        center: {x: e.clientX, y: clientY}
    };
    detonate(configChanges});
});
```

### Config example
The config and every config option is optional. It can be used for `explode()`, `prepare()` and `detonate()` functions. In a case of `detonate()` it will merge it with previously passed config to `prepare()` function.

```typescript
const config = {
    duration: 500, // animation duration
    center: {x: 123, y: 123}, // explosion center. default: target element center
    shouldRemoveEl: true, // toggle for element removal from DOM after explosion. default: false
    color: '#CCCCCC', // background color of shatter. default: background color of target element
    distance: 2, // shatter travel distance - multiplier of slice size. default: 2
    sliceCount: 10, // slice count in one axis. default: 10
    maxSliceSize: 15, // default: 15
    shatterClass: 'asdf' // default: none
};
// now call `detonate(config)` or `prepare(config)` or `detonate(optionalConfing)`
```
